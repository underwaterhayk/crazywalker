package com.kirakossiangames.crazywalker.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class UIStage extends Stage {
    public UIStage() {
        super();
        initViewPort();
    }

    private void initViewPort() {
        float vpWidth = 480;
        float vpHeight = vpWidth * Gdx.graphics.getHeight() / Gdx.graphics.getWidth();
        FitViewport viewport = new FitViewport(vpWidth, vpHeight);
        setViewport(viewport);
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
    }

    @Override
    public void draw() {
        super.draw();
        getViewport().apply();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
