package com.kirakossiangames.crazywalker.managers;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kirakossiangames.crazywalker.Facade;

public class GameSkinManager {

    private TextureRegion tileTextureRegion;

    public GameSkinManager() {
        tileTextureRegion = Facade.getGame().getRm().getTextureRegion("game-tile-basic");
    }
}
