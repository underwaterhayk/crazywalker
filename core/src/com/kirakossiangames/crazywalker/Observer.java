package com.kirakossiangames.crazywalker;

public interface Observer {
    String[] listNotificationInterests();
    void handleNotification(String notification, Object body);
}
