package com.kirakossiangames.crazywalker;

import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

public class Facade {
    private static Facade instance = null;
    private HashMap<String, Array<Observer>> observerMap;

    private CrazyWalker game;

    protected Facade() {
        observerMap = new HashMap<String, Array<Observer>>();
    }

    private static Facade get() {
        if(instance == null) {
            instance = new Facade();
        }
        return instance;
    }


    public static void sendNotification(String notification, Object body) {
        Array<Observer> observers = get().observerMap.get(notification);
        if (observers != null) {
            for (int i = observers.size-1; i >= 0; i--) {
                Observer observer = observers.get(i);
                observer.handleNotification(notification, body);

            }
        }

    }

    public static void registerObserver(Observer observer) {
        String[] noteInterests = observer.listNotificationInterests();
        for (String notificationName : noteInterests) {
            if (get().observerMap.get(notificationName) == null) {
                get().observerMap.put(notificationName, new Array());
            }
            Array<Observer> observers = get().observerMap.get(notificationName);
            if(!observers.contains(observer, true)) {
                observers.add(observer);
            }
        }
    }

    public static void unregisterObserver(Observer observer){
        HashMap<String, Array<Observer>> observersMap = get().observerMap;
        for (String notificationName : observersMap.keySet()){
            Array<Observer> observersArray = observersMap.get(notificationName);
            for (int i = observersArray.size - 1; i >= 0; i--) {
                if (observer == observersArray.get(i)){
                    observersArray.removeValue(observer, true);
                }
            }
        }
    }

    public static void setGame(CrazyWalker game) {
        get().game = game;
    }
    public static CrazyWalker getGame() {
        return get().game;
    }


    public static void dispose() {
        if(instance==null){
            return;
        }
        instance.observerMap.clear();
        instance.game = null;
        instance  = null;
    }
}
