package com.kirakossiangames.crazywalker.data;

/**
 * Created by kirakos on 12/29/17.
 */
public class SaveData {
    private int coins;
    private boolean tutorialPassed;
    private boolean musicOn =   true;
    private boolean soundOn =   true;

    private int gameCount;

    private boolean isPersonalizedAdsOn =   true;


    public SaveData() {

    }

    public void initialize() {

    }

    public boolean isTutorialPassed() {
        return tutorialPassed;
    }

    public void setTutorialPassed() {
        this.tutorialPassed = true;
    }


    public void addCoin() {
        addCoin(1);
    }
    public void addCoin(int count) {
        coins+=count;
    }

    public void setMusicOn(boolean musicOn) {
        this.musicOn = musicOn;
    }

    public void setSoundOn(boolean soundOn) {
        this.soundOn = soundOn;
    }

    public boolean isMusicOn() {
        return musicOn;
    }
    public boolean isSoundOn() {
        return soundOn;
    }

    public void toggleMusic() {
        setMusicOn(!isMusicOn());
    }

    public void toggleSound() {
        setSoundOn(!isSoundOn());
    }

    public int getCoins() {
        return coins;
    }

    public void setGameCount(int gameCount) {
        this.gameCount = gameCount;
    }

    public int getGameCount() {
        return gameCount;
    }

    public void addGameCount() {
        gameCount++;
    }

    public boolean canBuyCoins(int price) {
        return (coins>=price);
    }
    public void consumeCoins(int coins){
        this.coins-=coins;
        if(this.coins<0){
            this.coins  =   0;
        }
    }

    public boolean isPersonalizedAdsOn () {
        return isPersonalizedAdsOn;
    }

    public void setPersonalizedAdsOnOff (boolean onOff) {
        isPersonalizedAdsOn = onOff;
    }
}
