package com.kirakossiangames.crazywalker.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Json;

public class SaveDataManager {
    public static float SAVE_INTERVAL = 25; // sec
    private final String prefsKey = "CrazyWalker Preferences";

    private Json json;

    public Preferences prefs;

    private boolean saveScheduled;
    private float saveTimer;
    public SaveData saveData;

    public SaveDataManager() {
        json    =   new Json();
        prefs = Gdx.app.getPreferences("CrazyWalker");
        load();
    }
    public void act(float delta){
        saveTimer += delta;
        if (saveTimer >= SAVE_INTERVAL) {
            forceSave();
            saveTimer = 0;
        }
    }
    public void load(){
        String jsonString = prefs.getString(prefsKey);
//        try {
//            jsonString = AESEncryption.decrypt(jsonString);
//        } catch (Exception e) {
//            //e.printStackTrace();
//        }

        json.setIgnoreUnknownFields(true);
        saveData = json.fromJson(SaveData.class, jsonString);
        if (saveData == null) {
            saveData = new SaveData();
            saveData.initialize();
        }
    }
    public void save(){
        saveScheduled = true;
    }
    public void forceSave(){
        if (!saveScheduled) return;
        String dataString = json.toJson(saveData);
//        try {
//            dataString = AESEncryption.encrypt(dataString);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        prefs.putString(prefsKey, dataString);
        prefs.flush();
        saveScheduled = false;
    }


}
