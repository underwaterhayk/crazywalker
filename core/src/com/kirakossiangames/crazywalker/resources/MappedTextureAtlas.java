package com.kirakossiangames.crazywalker.resources;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;


public class MappedTextureAtlas extends TextureAtlas {
    HashMap<String, AtlasRegion> regionsHashMap;

    public MappedTextureAtlas(FileHandle packFile) {
        super(packFile);
        mapRegions();
    }

    public MappedTextureAtlas(TextureAtlasData data) {
        super(data);
        mapRegions();
    }

    private void mapRegions() {
        Array<AtlasRegion> regions = getRegions();
        regionsHashMap = new HashMap<String, AtlasRegion>(regions.size);
        for (int i = 0, n = regions.size; i < n; i++)
            regionsHashMap.put(regions.get(i).name,regions.get(i));

    }

    @Override
    public AtlasRegion findRegion(String name) {
        return regionsHashMap.get(name);
    }
}
