package com.kirakossiangames.crazywalker.resources;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.utils.Array;

/** {} to load {@link ParticleEffect} instances. Passing a {@link ParticleEffectLoaderMappedParameter} to
 * {@link AssetManager#load(String, Class, AssetLoaderParameters)} allows to specify an atlas file or an image directory to be
 * used for the effect's images. Per default images are loaded from the directory in which the effect file is found. */
public class ParticleEffectLoaderMapped extends SynchronousAssetLoader<ParticleEffect, ParticleEffectLoaderMapped.ParticleEffectLoaderMappedParameter> {
	public ParticleEffectLoaderMapped(FileHandleResolver resolver) {
		super(resolver);
	}

	@Override
	public ParticleEffect load (AssetManager am, String fileName, FileHandle file, ParticleEffectLoaderMappedParameter param) {
		ParticleEffect effect = new ParticleEffect();
		if (param != null && param.atlasFile != null)
			effect.load(file, am.get(param.atlasFile, MappedTextureAtlas.class), param.atlasPrefix);
		else if (param != null && param.imagesDir != null)
			effect.load(file, param.imagesDir);
		else
			effect.load(file, file.parent());
		return effect;
	}

	@Override
	public Array<AssetDescriptor> getDependencies (String fileName, FileHandle file, ParticleEffectLoaderMappedParameter param) {
		Array<AssetDescriptor> deps = null;
		if (param != null && param.atlasFile != null) {
			deps = new Array();
			deps.add(new AssetDescriptor<MappedTextureAtlas>(param.atlasFile, MappedTextureAtlas.class));
		}
		return deps;
	}

	/** Parameter to be passed to {@link AssetManager#load(String, Class, AssetLoaderParameters)} if additional configuration is
	 * necessary for the {@link ParticleEffect}. */
	public static class ParticleEffectLoaderMappedParameter extends AssetLoaderParameters<ParticleEffect> {
		/** Atlas file name. */
		public String atlasFile;
		/** Optional prefix to image names **/
		public String atlasPrefix;
		/** Image directory. */
		public FileHandle imagesDir;
	}
}
