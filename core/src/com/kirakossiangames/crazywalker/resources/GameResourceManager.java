package com.kirakossiangames.crazywalker.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by kirakos on 12/17/17.
 */
public class GameResourceManager {


    private AssetManager assetManager;

    private TextureAtlas textureAtlas;
    private ObjectMap<String, ParticleEffect> particles = new ObjectMap<String, ParticleEffect>();
    private ObjectMap<String, Music> musics = new ObjectMap<String, Music>();
    private ObjectMap<String, Sound> sounds = new ObjectMap<String, Sound>();



    private final String particlesPath  = "particles/";
    private final String musicPath      = "music/";
    private final String soundsPath     = "sounds/";
    private Array<String> particlesList =   new Array<String>();
    private Array<String> musicList     =   new Array<String>();
    private Array<String> soundsList    =   new Array<String>();
    private String audioExtension   =   ".mp3";

    public GameResourceManager( ) {
       assetManager =   new AssetManager();
//       initParticles();
//       initMusic();
//       initSounds();

    }

    private void initSounds() {
        soundsList.add("click");
        soundsList.add("boost");
        soundsList.add("take");
        soundsList.add("angel");
        soundsList.add("coins");
        soundsList.add("coin-take");
        soundsList.add("bonus-take");
        soundsList.add("lose");
        soundsList.add("timer-slow");
        soundsList.add("start");
    }

    private void initMusic() {
        musicList.add("menu");
        musicList.add("game1");
        musicList.add("game2");
        musicList.add("game3");
        musicList.add("game4");
    }

    private void initParticles() {
        particlesList.add("boost-effect");
        particlesList.add("explossion");
        particlesList.add("boost-item-particle");
        particlesList.add("coin-pe");
        particlesList.add("bonus-pe");
        particlesList.add("take-pe");
        particlesList.add("gold-rain");
        particlesList.add("coin-boost-complete-pe");
        particlesList.add("time-take-pe");
    }

    public void load(){

        assetManager.load("pack.atlas", TextureAtlas.class);
        ParticleEffectLoader.ParticleEffectParameter parameter  =   new ParticleEffectLoader.ParticleEffectParameter();

        parameter.atlasFile =  "pack.atlas";
        for (String resource : particlesList) {
            assetManager.load(particlesPath + resource, ParticleEffect.class, parameter);
        }
        for (String resource : soundsList) {
            assetManager.load(soundsPath + resource + audioExtension, Sound.class);
        }
        for (String resource : musicList) {
            assetManager.load(musicPath + resource + audioExtension, Music.class);
        }
    }
    public void init() {
        textureAtlas    =   assetManager.get( "pack.atlas", TextureAtlas.class);
        for (String resource : particlesList) {
            particles.put(resource, assetManager.get(particlesPath + resource, ParticleEffect.class));
        }
        for (String resource : soundsList) {
            sounds.put(resource, assetManager.get(soundsPath + resource + audioExtension, Sound.class));
        }
        for (String resource : musicList) {
            musics.put(resource, assetManager.get(musicPath + resource + audioExtension, Music.class));
        }
    }
    public TextureRegion getTextureRegion (String name) {
        return textureAtlas.findRegion(name);
    }
    public TextureAtlas.AtlasRegion getAtlasRegion (String name) {
        return textureAtlas.findRegion(name);
    }
    public ParticleEffect getParticleEffect (String name) {
        return particles.get(name);
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }
    public Array<TextureAtlas.AtlasRegion> getSpriteFrames(String name){
        Array<TextureAtlas.AtlasRegion> runningFrames = textureAtlas.findRegions(name);
        return runningFrames;

    }

    public Music getMusic(String name) {
        return musics.get(name);
    }

    public Sound getSound(String name) {
        return sounds.get(name);
    }
}
