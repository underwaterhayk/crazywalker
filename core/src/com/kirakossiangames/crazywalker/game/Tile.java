package com.kirakossiangames.crazywalker.game;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.utils.Pool;

public class Tile extends RenderableItem implements Pool.Poolable {


    public Tile() {

    }

    @Override
    public void reset() {

    }

    @Override
    public void render(Batch batch) {
        batch.draw();
    }
}
