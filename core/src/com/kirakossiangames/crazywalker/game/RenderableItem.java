package com.kirakossiangames.crazywalker.game;

import com.badlogic.gdx.graphics.g2d.Batch;

public abstract class RenderableItem {
    protected float x;
    protected float y;

    protected float width;

    protected float height;

    public abstract void render(Batch batch);

    public void setPosition(float x, float y) {
        setX(x);
        setY(y);
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
