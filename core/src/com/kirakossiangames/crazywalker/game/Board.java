package com.kirakossiangames.crazywalker.game;

import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

public class Board {

    private Pool<Tile> tilesPool;
    private Array<Tile> tilesArray;
    private float topTilesPosY = 0;

    public Board() {
        tilesArray = new Array<>();
        tilesPool = new Pool<Tile>() {
            @Override
            protected Tile newObject() {
                return createNewTile();
            }

            @Override
            protected void reset(Tile object) {
                super.reset(object);
            }
        };
    }

    private Tile createNewTile() {
        return new Tile();
    }

    public void createTilesRow(){
        int rowsAmount = 5;
        for (int i = 0; i < rowsAmount ; i++) {
            Tile tile = tilesPool.obtain();
            tilesArray.add(tile);
            tile.setPosition();
        }


    }

    public void render(PolygonSpriteBatch batch) {
        for (int i = 0; i < tilesArray.size; i++) {
            tilesArray.get(i).render(batch);

        }
    }
}
