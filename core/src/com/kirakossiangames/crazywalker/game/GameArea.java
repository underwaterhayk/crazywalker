package com.kirakossiangames.crazywalker.game;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;

public class GameArea implements InputProcessor {

    private Board board;
    private PolygonSpriteBatch batch;

    public GameArea() {
        batch = new PolygonSpriteBatch();
        board = new Board();
    }

    public void render() {
        board.render(batch);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (pointer != 1) {
            return false;
        }

        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }

}
