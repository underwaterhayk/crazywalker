package com.kirakossiangames.crazywalker.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.kirakossiangames.crazywalker.CrazyWalker;
import com.kirakossiangames.crazywalker.game.GameArea;
import com.kirakossiangames.crazywalker.stages.UIStage;

public class GameScreen implements Screen {
    private final InputMultiplexer inputMultiplexer;
    private final GameArea gameArea;
    private CrazyWalker game;
    private UIStage uiStage;

    public GameScreen(CrazyWalker game) {
        this.game = game;
        uiStage = new UIStage();
        gameArea = new GameArea();

        inputMultiplexer    =   new InputMultiplexer();
        inputMultiplexer.addProcessor(uiStage);
        inputMultiplexer.addProcessor(gameArea);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        uiStage.draw();
        gameArea.render();
    }

    public UIStage getUI() {
        return uiStage;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
