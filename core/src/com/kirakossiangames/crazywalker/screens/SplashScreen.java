package com.kirakossiangames.crazywalker.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kirakossiangames.crazywalker.CrazyWalker;

public class SplashScreen implements Screen {

    private final CrazyWalker game;
    private SpriteBatch spriteBatch;
    private TextureRegion splashFull;
    private float imgHeight;
    private float imgWidth;

    private boolean resourceManagerLoaded;
    private float timeOut;
    private boolean prepareStarted;

    public SplashScreen(CrazyWalker game) {
        this.game = game;
        spriteBatch = new SpriteBatch();
        splashFull = new TextureRegion(new Texture(Gdx.files.internal("splash/splashFull.jpg")));
        splashFull.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        calculateRatio();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.begin();
        spriteBatch.draw(splashFull, Gdx.graphics.getWidth()/2 - imgWidth/2, Gdx.graphics.getHeight()/2f - imgHeight/2, imgWidth, imgHeight);
        spriteBatch.end();

        boolean assetManagerCompletedLoading = game.getRm().getAssetManager().update(100);

        if (!assetManagerCompletedLoading) {


        } else {

            if (!resourceManagerLoaded) {
                game.getRm().init();
                resourceManagerLoaded = true;
            }

            if (!prepareStarted){
                game.prepareGame();
                prepareStarted = true;
            }
            //Loading is done, lets transition to the game screen
            if (timeOut > 0.4f){
                game.setGameScreen();

            }
            timeOut+=delta;

        }
    }

    private void calculateRatio() {
        float imgRatio = splashFull.getRegionWidth()*1f/splashFull.getRegionHeight()*1f;
        float screenRatio = Gdx.graphics.getWidth()*1f/ Gdx.graphics.getHeight()*1f;
        if (imgRatio > screenRatio){
            // screen is taller so base is height
            imgHeight = Gdx.graphics.getHeight();
            float ratio = 1f* Gdx.graphics.getHeight()/splashFull.getRegionHeight();
            imgWidth = ratio*splashFull.getRegionWidth();
        } else {
            // image is taller so base is width
            imgWidth = Gdx.graphics.getWidth();
            float ratio = 1f* Gdx.graphics.getWidth()/splashFull.getRegionWidth();
            imgHeight = ratio*splashFull.getRegionHeight();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
