package com.kirakossiangames.crazywalker;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.kirakossiangames.crazywalker.data.GameData;
import com.kirakossiangames.crazywalker.data.SaveDataManager;
import com.kirakossiangames.crazywalker.managers.GameSkinManager;
import com.kirakossiangames.crazywalker.resources.GameResourceManager;
import com.kirakossiangames.crazywalker.screens.GameScreen;
import com.kirakossiangames.crazywalker.screens.SplashScreen;

public class CrazyWalker extends Game {
	private GameScreen gameScreen;
	private SplashScreen splashScreen;
	private Screen activeScreen;


	private GameResourceManager rm;
	private GameData gameData;
	private SaveDataManager saveDataManager;
	private GameSkinManager gameSkinManager;

	@Override
	public void create() {
		setSplashScreen();
		initRm();
		loadRm();
	}

	public void prepareGame() {
		Facade.setGame(this);
		gameData = new GameData();
		saveDataManager = new SaveDataManager();
		gameSkinManager = new GameSkinManager();
	}

	public void loadRm() {
		rm.load();
	}

	public void initRm() {
		rm = new GameResourceManager();
	}

	private void setSplashScreen() {
		splashScreen = new SplashScreen(this);
		setScreen(splashScreen);
		activeScreen = splashScreen;
	}

	public void setGameScreen() {
		gameScreen = new GameScreen(this);
		setScreen(gameScreen);
		activeScreen = gameScreen;
	}

	public GameResourceManager getRm() {
		return rm;
	}

	public GameData getGameData() {
		return gameData;
	}

	public SaveDataManager getSaveDataManager() {
		return saveDataManager;
	}

	public GameScreen getGameScreen() {
		return gameScreen;
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		activeScreen.dispose();
	}
}
